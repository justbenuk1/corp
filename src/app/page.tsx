import HomeImage from '@/../public/home.jpg'
import Hero from '@/components/ui/hero';

export default function Home() {
  return (
    <Hero title='Professional Cloud Hosting' image={HomeImage} alt='Car Garage' />
  );
}
