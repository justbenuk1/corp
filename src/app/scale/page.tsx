import ScaleImg from '../../../public/scale.jpg'
import Hero from '@/components/ui/hero'
export default function ScalePage() {
  return (
  <Hero image={ScaleImg} alt='another alt image' title='Scale your app infinatly'/>
  )
}
