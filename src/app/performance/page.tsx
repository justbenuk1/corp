import PerImg from '../../../public/performance.jpg'
import Hero from '@/components/ui/hero'
export default function PerformancePage(){
  return (
  <Hero image={PerImg} alt='perfomance' title='We serve high performance applications'/>
  )
}
