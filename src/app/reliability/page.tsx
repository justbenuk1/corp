import RelImg from '../../../public/reliability.jpg'
import Hero from '@/components/ui/hero'
export default function ReliabiltyPage(){
  return (
  <Hero image={RelImg} alt='Reliability Image' title='Super high reliability hosting'/>
  )
}
